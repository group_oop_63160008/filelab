package com.mycompany.FileLab;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

import com.mycompany.FileLab.Friend;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class WriteFreind {

    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend friend1 = new Friend("Nontalee", 21, "0853861336");
            Friend friend2 = new Friend("Jakkaman", 45, "0887654321");
            Friend friend3 = new Friend("Moowhan", 45, "089765432");
            // friends.dat
            File file = new File("friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friend1);
            oos.writeObject(friend2);
            oos.writeObject(friend3);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFreind.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFreind.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFreind.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}